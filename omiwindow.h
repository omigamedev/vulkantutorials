#pragma once

#include <Windows.h>

#include <map>
#include <functional>

namespace omi
{
	class Window
	{
	private:
		static std::map<HWND, Window*> _registeredWindows;
		static LRESULT CALLBACK _MessageHandler(HWND hWnd, UINT msgID, WPARAM wp, LPARAM lp);
		LRESULT HandleMessage(HWND hWnd, UINT msgID, WPARAM wp, LPARAM lp);

	public:
		HWND handle;
		int w, h;
		std::function<void(int key)> keyDown;

		Window();
		virtual ~Window();

		bool Create(int width, int height);
	};

}
