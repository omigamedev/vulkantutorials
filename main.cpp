#define VK_USE_PLATFORM_WIN32_KHR
#define VK_PROTOTYPES
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <vulkan\vulkan.h>
#include <vector>

#include "omiWindow.h"

#pragma comment (lib, "vulkan-1.lib")
#pragma comment (linker, "/subsystem:windows")

template <typename T, typename FirstParam, typename F>
std::vector<T> Enumerate(F function, FirstParam ctx)
{
    uint32_t count = 0;
    function(ctx, &count, nullptr);
    std::vector<T> ret{ count };
    function(ctx, &count, ret.data());
    return ret;
}

template <typename T, typename FirstParam, typename SecondParam, typename F>
std::vector<T> Enumerate(F function, FirstParam ctx, SecondParam p2)
{
    uint32_t count = 0;
    function(ctx, p2, &count, nullptr);
    std::vector<T> ret{ count };
    function(ctx, p2, &count, ret.data());
    return ret;
}

void print(const VkPhysicalDeviceFeatures& o)
{
    printf("- robustBufferAccess = %d\n", o.robustBufferAccess);
    printf("- fullDrawIndexUint32 = %d\n", o.fullDrawIndexUint32);
    printf("- imageCubeArray = %d\n", o.imageCubeArray);
    printf("- independentBlend = %d\n", o.independentBlend);
    printf("- geometryShader = %d\n", o.geometryShader);
    printf("- tessellationShader = %d\n", o.tessellationShader);
    printf("- sampleRateShading = %d\n", o.sampleRateShading);
    printf("- dualSrcBlend = %d\n", o.dualSrcBlend);
    printf("- logicOp = %d\n", o.logicOp);
    printf("- multiDrawIndirect = %d\n", o.multiDrawIndirect);
    printf("- drawIndirectFirstInstance = %d\n", o.drawIndirectFirstInstance);
    printf("- depthClamp = %d\n", o.depthClamp);
    printf("- depthBiasClamp = %d\n", o.depthBiasClamp);
    printf("- fillModeNonSolid = %d\n", o.fillModeNonSolid);
    printf("- depthBounds = %d\n", o.depthBounds);
    printf("- wideLines = %d\n", o.wideLines);
    printf("- largePoints = %d\n", o.largePoints);
    printf("- alphaToOne = %d\n", o.alphaToOne);
    printf("- multiViewport = %d\n", o.multiViewport);
    printf("- samplerAnisotropy = %d\n", o.samplerAnisotropy);
    printf("- textureCompressionETC2 = %d\n", o.textureCompressionETC2);
    printf("- textureCompressionASTC_LDR = %d\n", o.textureCompressionASTC_LDR);
    printf("- textureCompressionBC = %d\n", o.textureCompressionBC);
    printf("- occlusionQueryPrecise = %d\n", o.occlusionQueryPrecise);
    printf("- pipelineStatisticsQuery = %d\n", o.pipelineStatisticsQuery);
    printf("- vertexPipelineStoresAndAtomics = %d\n", o.vertexPipelineStoresAndAtomics);
    printf("- fragmentStoresAndAtomics = %d\n", o.fragmentStoresAndAtomics);
    printf("- shaderTessellationAndGeometryPointSize = %d\n", o.shaderTessellationAndGeometryPointSize);
    printf("- shaderImageGatherExtended = %d\n", o.shaderImageGatherExtended);
    printf("- shaderStorageImageExtendedFormats = %d\n", o.shaderStorageImageExtendedFormats);
    printf("- shaderStorageImageMultisample = %d\n", o.shaderStorageImageMultisample);
    printf("- shaderStorageImageReadWithoutFormat = %d\n", o.shaderStorageImageReadWithoutFormat);
    printf("- shaderStorageImageWriteWithoutFormat = %d\n", o.shaderStorageImageWriteWithoutFormat);
    printf("- shaderUniformBufferArrayDynamicIndexing = %d\n", o.shaderUniformBufferArrayDynamicIndexing);
    printf("- shaderSampledImageArrayDynamicIndexing = %d\n", o.shaderSampledImageArrayDynamicIndexing);
    printf("- shaderStorageBufferArrayDynamicIndexing = %d\n", o.shaderStorageBufferArrayDynamicIndexing);
    printf("- shaderStorageImageArrayDynamicIndexing = %d\n", o.shaderStorageImageArrayDynamicIndexing);
    printf("- shaderClipDistance = %d\n", o.shaderClipDistance);
    printf("- shaderCullDistance = %d\n", o.shaderCullDistance);
    printf("- shaderFloat64 = %d\n", o.shaderFloat64);
    printf("- shaderInt64 = %d\n", o.shaderInt64);
    printf("- shaderInt16 = %d\n", o.shaderInt16);
    printf("- shaderResourceResidency = %d\n", o.shaderResourceResidency);
    printf("- shaderResourceMinLod = %d\n", o.shaderResourceMinLod);
    printf("- sparseBinding = %d\n", o.sparseBinding);
    printf("- sparseResidencyBuffer = %d\n", o.sparseResidencyBuffer);
    printf("- sparseResidencyImage2D = %d\n", o.sparseResidencyImage2D);
    printf("- sparseResidencyImage3D = %d\n", o.sparseResidencyImage3D);
    printf("- sparseResidency2Samples = %d\n", o.sparseResidency2Samples);
    printf("- sparseResidency4Samples = %d\n", o.sparseResidency4Samples);
    printf("- sparseResidency8Samples = %d\n", o.sparseResidency8Samples);
    printf("- sparseResidency16Samples = %d\n", o.sparseResidency16Samples);
    printf("- sparseResidencyAliased = %d\n", o.sparseResidencyAliased);
    printf("- variableMultisampleRate = %d\n", o.variableMultisampleRate);
    printf("- inheritedQueries = %d\n", o.inheritedQueries);
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR cmdLine, int showCmd)
{
    AllocConsole();
    AttachConsole(GetCurrentProcessId());
    freopen("CONOUT$", "w", stdout);
    SetConsoleTitle("Hello");

    const char *exts[] = {
        VK_KHR_SURFACE_EXTENSION_NAME,
        VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
    const  VkApplicationInfo app_info = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO, nullptr, // Type, Next
        "Vulkan00", 0, // Application name, version
        "Vulkan00", 0, // Engine name, version
        VK_API_VERSION
    };
    VkInstanceCreateInfo ic_info = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, nullptr, // Type, Next
        0, &app_info, // flags, app info ptr
        0, nullptr,   // layers
        2, exts       // extensions
    };

    uint32_t inst_ext_count = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &inst_ext_count, nullptr);
    std::vector<VkExtensionProperties> inst_ext{ inst_ext_count };
    vkEnumerateInstanceExtensionProperties(nullptr, &inst_ext_count, inst_ext.data());
    for (const auto& xp : inst_ext)
    {
        printf("Instance Extension name: %s\n", xp.extensionName);
    }
    VkInstance instance;
    vkCreateInstance(&ic_info, nullptr, &instance);

    uint32_t phys_devs_count = 0;
    vkEnumeratePhysicalDevices(instance, &phys_devs_count, nullptr);
    std::vector<VkPhysicalDevice> phys_devs{ phys_devs_count };
    vkEnumeratePhysicalDevices(instance, &phys_devs_count, phys_devs.data());
    for (const auto& dev : phys_devs)
    {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(dev, &properties);
        printf("Device name: %s\n", properties.deviceName);

        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(dev, &features);
        print(features);

        uint32_t qprops_count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(dev, &qprops_count, nullptr);
        std::vector<VkQueueFamilyProperties> qprops{ qprops_count };
        vkGetPhysicalDeviceQueueFamilyProperties(dev, &qprops_count, qprops.data());
        for (const auto& qp : qprops)
        {
            printf("Queue flags: ");
            if (qp.queueFlags & VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT) printf(" VK_QUEUE_GRAPHICS_BIT");
            if (qp.queueFlags & VkQueueFlagBits::VK_QUEUE_COMPUTE_BIT) printf(" VK_QUEUE_COMPUTE_BIT");
            if (qp.queueFlags & VkQueueFlagBits::VK_QUEUE_SPARSE_BINDING_BIT) printf(" VK_QUEUE_SPARSE_BINDING_BIT");
            if (qp.queueFlags & VkQueueFlagBits::VK_QUEUE_TRANSFER_BIT) printf(" VK_QUEUE_TRANSFER_BIT");
        }
        printf("\n");
    }

    // Chose a GPU to use
    VkPhysicalDevice gpu = phys_devs[0];

    uint32_t xprops_count = 0;
    vkEnumerateDeviceExtensionProperties(gpu, nullptr, &xprops_count, nullptr);
    std::vector<VkExtensionProperties> xprops{ xprops_count };
    vkEnumerateDeviceExtensionProperties(gpu, nullptr, &xprops_count, xprops.data());
    for (const auto& xp : xprops)
    {
        printf("Device Extension name: %s\n", xp.extensionName);
    }

    omi::Window wnd;
    wnd.Create(800, 600);

    VkSurfaceKHR surface;
    VkWin32SurfaceCreateInfoKHR createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    createInfo.pNext = NULL;
    createInfo.flags = 0;
    createInfo.hinstance = hInst;
    createInfo.hwnd = wnd.handle;
    if (vkCreateWin32SurfaceKHR(instance, &createInfo, NULL, &surface) != VkResult::VK_SUCCESS)
    {
        printf("surface failed\n");
    }

    VkBool32 supported = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(gpu, 0, surface, &supported);
    if (!supported)
    {
        printf("present not supported\n");
    }

    VkDeviceQueueCreateInfo queue_info;
    float queue_info_priorities[] = { 1.f };
    queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_info.pNext = nullptr;
    queue_info.flags = 0;
    queue_info.queueFamilyIndex = 0;
    queue_info.queueCount = 1;
    queue_info.pQueuePriorities = queue_info_priorities;

    VkDeviceCreateInfo device_info;
    device_info.sType = VkStructureType::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_info.pNext = nullptr;
    device_info.queueCreateInfoCount = 1;
    device_info.pQueueCreateInfos = &queue_info;
    device_info.enabledLayerCount = 0;
    device_info.ppEnabledLayerNames = nullptr;
    device_info.enabledExtensionCount = 0;
    device_info.ppEnabledExtensionNames = nullptr;


    MSG msg;
    while (true)
    {
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            // render
        }
    }

    vkDestroyInstance(instance, nullptr);

    return 0;
}